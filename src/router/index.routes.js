const routes = require('express').Router();
const appRouter = require('./app.routes');
routes.use('/app', appRouter);

module.exports = routes;