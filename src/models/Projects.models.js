const { model, Schema, Types } = require('mongoose');

const ProjectsSchema = new Schema({
    name: {
        type: String,
        require: true,
        unique: true
    },
    status: {
        type: String,
        enum: ['Sin registros cargados', 'Procesando', 'Carga completada'],
        default: 'Sin registros cargados'
    },
    clientDataId: [{
        type: Types.ObjectId,
        ref: "ClientsData"
    }]
},
    {
        timestamps: true,
        timeseries: false,
        versionKey: false,
        collection: "Projects"
    }
);

const Projects = model('Projects', ProjectsSchema);

module.exports = Projects;