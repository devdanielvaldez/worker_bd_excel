const { model, Schema, Types } = require('mongoose');

const UsersSchema = new Schema({
    userId: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    rol:  {
        type: Number,
        required: true,
        enum: [1, 2], //1 = Admin 2 = General
        default: 2
    },
    name: {
        type: String,
        required: true
    }
},
    {
        timestamps: true,
        timeseries: false,
        versionKey: false,
        collection: "Users"
    }
);

const Users = model('Users', UsersSchema);
 
module.exports = Users;