const { model, Schema, Types } = require('mongoose');

const CommentsSchema = new Schema({
    comment: {
        type: String,
        require: true
    },
    userId: {
        type: Types.ObjectId,
        ref: "Users"
    }
},
    {
        timestamps: true,
        timeseries: false,
        versionKey: false,
        collection: "Comments"
    }
);

const Comments = model('Comments', CommentsSchema);
module.exports = Comments;