const { model, Schema, Types } = require('mongoose');

const ClientsDataSchema = new Schema({
    verificada: String,
    cedula: String,
    email: String,
    nombre: String,
    apellido: String,
    genero: String,
    edad: String,
    nContacto: String,
    fNacimiento: String,
    provincia: String,
    direccion: String,
    ciudad: String,
    fase: String,
    nombreProyecto: String,
    objectivoProyecto: String,
    descripcionProyecto: String,
    tipoProyecto: String,
    poblacion: String,
    ubicacion: String,
    tipoAcomp: String,
    financiado: String,
    financia: String,
    capacitaciones: String,
    diaCapacitaciones: String,
    horarioCapacitaciones: String,
    temaRelacionado: String,
    nivelAcademico: String,
    accesoInternet: String,
    confirmRedesSociales: String,
    redesSociales: String,
    empresaConstituida: String,
    registroNacional: String,
    rnc: String,
    documentos: String,
    queHaceProyecto: String,
    impactoProyecto: String,
    nEmpleos: String,
    dineroFinanciamiento: String,
    monto: String,
    institucionFinanciera: String,
    observaciones: String,
    tipoEmpresaLab: String,
    tags: String,
    commentsId: [{
        type: Types.ObjectId,
        ref: "Comments"
    }]
},
    {
        timestamps: true,
        timeseries: false,
        versionKey: false,
        collection: "ClientsData"
    }
);

const ClientsData = model('ClientsData', ClientsDataSchema);

module.exports = ClientsData;