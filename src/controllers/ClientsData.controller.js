const Projects = require('../models/Projects.models');
const ClientsData = require('../models/ClientsData.models');


function saveData(data, projectId) {
        console.log('entro al proceso')
        Projects.findByIdAndUpdate(projectId, {
            status: 'Procesando'
        })
        .then(() => {
            data.forEach(async (element) => {
                const newClientsData = new ClientsData(element);
                await newClientsData.save((err, data) => {
                    console.log('entro');
                    Projects.findByIdAndUpdate(projectId, {
                        $push: {
                            clientDataId: data._id
                        },
                        status: 'Carga completada'
                    })
                    .then(() => {
                        // process.exit();
                        console.log('termino');
                        return;
                    });
                })
            });
        });
}

exports.RegisterClientsData = async(req, res) => {
    try {
        res.send({
            ok: true,
            msg: "Los datos cargados estan siendo procesados..."
        });
        const { data, projectId } = req.body;
        saveData(data, projectId);


    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Se ha producido un error inesperado, por favor contacte al administrador del sistema."
        });
    }
}

