const express = require('express'),
      morgan = require('morgan'),
      cors = require('cors'),
      { config } = require('dotenv'),
      dbConnection = require('./db.connection');
config();

const app = express();

dbConnection.connection();

// app.use(JobsApi);
app.use(cors());
app.use(morgan('dev'));
app.use(express.json({ limit: '500mb' }));
app.use(express.urlencoded({ limit: '500mb', extended: true }));
app.use('/api', require('./src/router/index.routes'));

app.listen(process.env.PORT || 5200, () => {
    return console.log('---!---| -> API Running in port 5200');
})